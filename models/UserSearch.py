from sqlalchemy import true

from models.Shared import db

class UserSearch(db.Model):
    __tablename__= "usersearch"
    session_id = db.Column("session_id", db.String(255), primary_key=true)
    water_level = db.Column("water_level", db.String(255))
    time = db.Column("time", db.String(255))
    date = db.Column("date", db.String(255))
    yes_count = db.Column("yes_count", db.Integer, nullable=False, default=0)
    no_count = db.Column("no_count", db.Integer, nullable=False,default=0)

    def __repr__(self):
        return f"<UserSearch {self.id}>"
