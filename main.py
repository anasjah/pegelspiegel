import json
import traceback
import urllib.error
import urllib.parse
import urllib.request
from urllib.parse import quote

from dateutil import parser
from flask import Flask, make_response, request, jsonify
from flask_assistant import Assistant

from models.Shared import db
from models.UserSearch import UserSearch

app = Flask(__name__)
assist = Assistant(app, route='/ga')
app.config.from_pyfile('config.py')

db.app = app
db.init_app(app)


def call_link(session_id):
    responseMsg = "Falsche Angabe"
    db_handler = UserSearch.query
    pegel = db_handler.filter_by(session_id=session_id).first()
    print("date: " + pegel.date)
    print("river: " + pegel.water_level)
    print("time: " + pegel.time)
    if pegel.date != None and pegel.time != None:
        datetime_object = parser.parse(pegel.date + " " + pegel.time).isoformat()
        link = "https://www.pegelonline.wsv.de/webservices/rest-api/v2/stations/" + quote(
            pegel.water_level) + "/W/measurements.json?end=" + quote(datetime_object)
        print("Rest link : " + link)
        try:
            print("URL : " + link)
            with urllib.request.urlopen(link) as url:
                allWaterlevels = url.read()
                allWaterlevelsAsString = allWaterlevels.decode("utf8")
                allWaterlevelsAsJson = json.loads(allWaterlevelsAsString)
                if (len(allWaterlevelsAsJson) > 0):
                    simpleTime = parser.parse(pegel.time).time().isoformat()
                    responseMsg = "Beim Pegel " + pegel.water_level + " am " + pegel.date \
                                  + " um " + "%s" % (simpleTime) + " lag der Wasserstand bei " + str(
                        int(allWaterlevelsAsJson[len(allWaterlevelsAsJson) - 1]["value"])) \
                                  + " Zentimeter" + ". möchten Sie weitere Uhrzeit ?!"

                else:
                    responseMsg = "Leider liegen die Informationen über den gesuchten Pegel nicht vor ."

        except Exception as e:  # server error
            if isinstance(e, urllib.error.HTTPError):
                errorString = str(e.read())
                print("Error String \t" + errorString)
                if '"status":404' in errorString:
                    responseMsg = "Es gibt keinen Eintrag über den gewünschten Pegel, bitte prüfen Sie den Pegelnamen !"
                else:
                    responseMsg = "Leider besteht gerade einen Serverfehler, bitte vesuchen Sie mal später"
                    traceback.print_exc()
            else:
                responseMsg = "Leider besteht gerade einen Serverfehler, bitte vesuchen Sie mal später"
                traceback.print_exc()
    if pegel:
        pegel.yes_count = 0
        pegel.no_count = 0
        db.session.commit()
    return responseMsg


@app.route("/alexa", methods=["POST"])
def main():
    responseMsg = "Falsche Angabe"
    shouldEndSession = False
    db_handler = UserSearch.query
    jsonDict = {
        'version': '1.0'
    }

    data = request.json["request"]

    session = request.json["session"]

    pegel = db_handler.filter_by(session_id=session["sessionId"]).first()
    if not pegel:
        pegel = UserSearch()
        pegel.session_id = session["sessionId"]
        db.session.add(pegel)
        db.session.commit()

    if data["type"] == "LaunchRequest":
        responseMsg = "Herzlich Willkommen zum Pegelspiegel, Sagen Sie bitte Pegel gefolgt mit dem Pegelnamen"

    elif data["type"] == "IntentRequest":
        print("incoming data " + str(data))
        if "intent" in data:
            if "name" in data["intent"]:
                if "AMAZON.YesIntent" == data["intent"]["name"]:
                    pegel = db_handler.filter_by(session_id=session["sessionId"]).first()
                    pegel.yes_count = pegel.yes_count + 1
                    db.session.commit()
                    if (pegel.yes_count == 1 and pegel.no_count == 0):
                        responseMsg = "Sagen Sie Uhrzeit gefolgt mit der gewünschten Uhrzeit."
                    elif (pegel.yes_count == 2 and pegel.no_count == 0) or (
                            pegel.yes_count == 1 and pegel.no_count == 1):
                        responseMsg = "Sagen Sie Datum gefolgt mit dem gewünschten Datum."
                    elif (pegel.yes_count == 1 and pegel.no_count == 2):
                        responseMsg = "Sagen Sie Pegel gefolgt mit dem Pegelnamen"
                elif "AMAZON.NoIntent" == data["intent"]["name"]:
                    pegel = db_handler.filter_by(session_id=session["sessionId"]).first()
                    pegel.no_count = pegel.no_count + 1
                    db.session.commit()
                    if (pegel.no_count == 1):
                        responseMsg = "Möchten Sie weiteres Datum ?"
                    elif (pegel.no_count == 2):
                        responseMsg = "Möchten Sie weiteren Pegel ?"
                    elif (pegel.no_count == 3):
                        responseMsg = "Danke für Ihren Besuch. auf Wiedersehen !"
                        shouldEndSession = True
                        db.session.delete(pegel)
                        db.session.commit()
                elif "NewTimeIntent" == data["intent"]["name"]:
                    pegel = db_handler.filter_by(session_id=session["sessionId"]).first()
                    if "slots" in data["intent"]:
                        intentSlots = data["intent"]["slots"]
                        simpleTime = parser.parse(intentSlots["time"]["value"])
                        pegel.time = "%s" % (simpleTime.time().isoformat())
                        db.session.commit()
                        responseMsg = call_link(session["sessionId"])
                elif "NewDateIntent" == data["intent"]["name"]:
                    pegel = db_handler.filter_by(session_id=session["sessionId"]).first()
                    if "slots" in data["intent"]:
                        intentSlots = data["intent"]["slots"]
                        date = intentSlots["date"]["value"]
                        pegel.date = date
                        db.session.commit()
                        responseMsg = call_link(session["sessionId"])
                elif "PegelAssistentIntent" == data["intent"]["name"]:
                    if "slots" in data["intent"]:
                        intentSlots = data["intent"]["slots"]
                        if "date" in intentSlots:
                            pegel.date = intentSlots["date"]["value"]
                        if "river" in intentSlots:
                            pegel.water_level = intentSlots["river"]["value"]
                        if "time" in intentSlots:
                            simpleTime = parser.parse(intentSlots["time"]["value"]).time().isoformat()
                            pegel.time = "%s" % (simpleTime)
                        db.session.commit()
                        responseMsg = call_link(session["sessionId"])

    jsonDict["response"] = {
        "outputSpeech": {
            "type": "PlainText",
            "text": responseMsg
        },
        "shouldEndSession": shouldEndSession
    }

    resp = make_response(json.dumps(jsonDict))
    resp.headers["POST"] = "HTTP/1.1 200 OK"
    resp.headers["Content-Type"] = "application/json;charset=UTF-8"
    resp.headers["Content-Length"] = ""
    return resp


@app.route('/gassistant', methods=['POST'])
def get_movie_detail():
    data = request.get_json(silent=True)

    response = "Falsche Angabe"
    session_id = request.json["session"]
    db_handler = UserSearch.query
    pegel = db_handler.filter_by(session_id=session_id).first()
    if not pegel:
        pegel = UserSearch()
        pegel.session_id = session_id
        db.session.add(pegel)
        db.session.commit()
    try:
        action = data['queryResult']['action']

        if 'input.welcome' in action:
            response = "Herzlich Willkommen zum Pegelspiegel, Sagen Sie bitte Pegel gefolgt mit dem Pegelnamen"
        elif 'input.findriver' in action:
            parameters = data['queryResult']['parameters']
            simpleTime = parser.parse(str.split(parameters['time'], 'T')[1]).time().isoformat()
            pegel.water_level = parameters['river']
            pegel.date = str.split(parameters['date'], 'T')[0]
            pegel.time = "%s" % (simpleTime)
            response = call_link(session_id)
            print(str(response))
        elif 'input.yes' in action:
            pegel = db_handler.filter_by(session_id=session_id).first()
            if 'douple' in action:
                pegel.yes_count = pegel.yes_count + 2
            elif 'triple' in action:
                pegel.yes_count = pegel.yes_count + 3
            else:
                pegel.yes_count = pegel.yes_count + 1
            db.session.commit()
            if pegel.yes_count == 1 and pegel.no_count == 0:
                response = "Sagen Sie Uhrzeit gefolgt mit der gewünschten Uhrzeit."
            elif (pegel.yes_count == 2 and pegel.no_count == 0) or (
                    pegel.yes_count == 1 and pegel.no_count == 1):
                response = "Sagen Sie Datum gefolgt mit dem gewünschten Datum."
            elif (pegel.yes_count == 3 and pegel.no_count == 0) or (pegel.yes_count == 1 and pegel.no_count == 2):
                response = "Sagen Sie Pegel gefolgt mit dem Pegelnamen"
        elif 'input.no' in action:
            pegel = db_handler.filter_by(session_id=session_id).first()
            pegel.no_count = pegel.no_count + 1
            db.session.commit()
            if (pegel.no_count == 1):
                response = "Möchten Sie weiteres Datum ?"
            elif (pegel.no_count == 2):
                response = "Möchten Sie weiteren Pegel ?"
            elif (pegel.no_count == 3):
                db.session.delete(pegel)
                db.session.commit()
                response = "Danke für Ihren Besuch. auf Wiedersehen !"
        elif 'input.newtime' in action:
            parameters = data['queryResult']['parameters']
            simpleTime = parser.parse(str.split(parameters['time'], 'T')[1]).time().isoformat()
            pegel = db_handler.filter_by(session_id=session_id).first()
            pegel.time = "%s" % (simpleTime)
            db.session.commit()
            response = call_link(session_id)
        elif 'input.newdate' in action:
            parameters = data['queryResult']['parameters']
            pegel = db_handler.filter_by(session_id=session_id).first()
            pegel.date = str.split(parameters['date'], 'T')[0]
            db.session.commit()
            response = call_link(session_id)

    except Exception as e:
        traceback.print_exc()

    reply = {
        "fulfillmentText": response,
    }

    return jsonify(reply)


if __name__ == '__main__':
    app.run()
