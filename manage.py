from flask_script import Manager
from main import app
from flask_migrate import Migrate, MigrateCommand
from models.Shared import db

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)

manager.run()