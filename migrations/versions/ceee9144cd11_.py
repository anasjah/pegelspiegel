"""empty message

Revision ID: ceee9144cd11
Revises: 
Create Date: 2019-03-25 12:49:57.570829

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ceee9144cd11'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('usersearch',
    sa.Column('session_id', sa.String(length=255), nullable=False),
    sa.Column('water_level', sa.String(length=255), nullable=True),
    sa.Column('time', sa.String(length=255), nullable=True),
    sa.Column('date', sa.String(length=255), nullable=True),
    sa.PrimaryKeyConstraint('session_id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('usersearch')
    # ### end Alembic commands ###
